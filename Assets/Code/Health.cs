﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Health : NetworkBehaviour
{
    public int MaxHealth;

    // SyncVar: This makes the variable synchronized, which means it is updated and handled on the server
    // (hook = "OnHealthChanged") : The hook tells what method should be called whenever the SyncVar changes
    [SyncVar(hook = "OnHealthChanged")]
    public int CurrentHealth;
    public Text HealthScore;

    // Start is called before the first frame update
    void Start()
    {
        HealthScore.text = CurrentHealth.ToString();
    }

    public void TakeDamage(int howMuch)
    {
        if (!isServer)
        {
            return;
        }

        var newHealth = CurrentHealth - howMuch;

        if (newHealth <= 0)
        {
            //Debug.Log("Dead");
            CurrentHealth = MaxHealth;
            RpcDeath();
        }
        else
        {
            CurrentHealth = newHealth;

        }
    }

    void OnHealthChanged(int CurrentHealth, int updatedHealth)
    {
        HealthScore.text = updatedHealth.ToString();
    }

    // Rpc stands for remote procedure control (it is the reversed function of commands)
    // Rpc is issued on the server, but executed on the client
    [ClientRpc]
    void RpcDeath()
    {
        if (isLocalPlayer)
        {
            Debug.Log("Local Player");
            //transform.position = Vector3.zero;
            var spawnPoints = FindObjectsOfType<NetworkStartPosition>();
            var chosenPoint = Random.Range(0, spawnPoints.Length);
            transform.position = spawnPoints[chosenPoint].transform.position;
        }
    }
}
