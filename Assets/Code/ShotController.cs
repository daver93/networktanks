﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotController : MonoBehaviour
{
    public float SecondsBeforeDestroy;

    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, SecondsBeforeDestroy);
    }

    private void OnCollisionEnter(Collision other)
    {
        Destroy(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
